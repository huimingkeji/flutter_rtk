# flutter_rtk

A new Flutter project.


# 生成启动屏
flutter_native_splash: 2.0.0

flutter_native_splash:
    image: assets/launcher/splash.png
    color: "#ffffff"

flutter pub run flutter_native_splash:create



# 图标
flutter_launcher_icons: any

flutter_icons:
    android: "launcher_icon"
    ios: true
    image_path: "assets/launcher/icon.png"
    min_sdk_android: 21 # android min sdk min:16, default 21

flutter pub run flutter_launcher_icons:main

# android打包
flutter build apk --release --no-tree-shake-icons
flutter build apk --debug --no-tree-shake-icons