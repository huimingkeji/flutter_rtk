class RespDTO<T> {
  RespDTO({
    required this.code,
    required this.msg,
    required this.success,
    this.data,
  });

  RespDTO.fromJson(dynamic json) {
    code = json['code'];
    success = json['success'] ?? code == 200;
    data = json['data'];
    msg = json['msg'];
  }

  int code = 200;
  bool success = true;
  T? data;
  String msg = "操作成功";

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = code;
    map['success'] = code == 200;
    if (data != null) {
      map['data'] = data;
    }
    map['msg'] = msg;
    return map;
  }
}
