import 'package:flutter/material.dart';

class AppConstants {
  // base
  // static const String baseUrl = StoreService.to.getString("key");
  static const String baseUrl = "BASE_URL";

  // constants

  static const String deviceIsFirstOpen = "DEVICE_FIRST_OPEN";
  static const String userIsLogin = "USER_IS_LOGIN";
  static const String loginUserName = "LOGIN_USER_NAME";
  static const String loginPassword = "LOGIN_PASSWORD";

  // static const String autoLogin = "IS_AUTO_LOGIN";
  // static const String autoLoginTime = "AUTO_LOGIN_TIME";
  static const String token = "LOGIN_TOKEN";
  static const String tokenExpireTime = "LOGIN_TOKEN_EXPIRE_TIME";

  // static const String loginInfo = "LOGIN_INFO";
  // static const String currentPostMode = "CURRENT_POST_MODE";

  // // aes key
  // static const String aesKey = "thanks,pig4cloud";
  //
  // // form login client
  // static const String formLoginClient = "erpbizapp";

  // color
  static const Color brandColor = Color(0xFF007AFF);
  static const Color pageBackgroundColor = Color(0xFFF4F4F4);
}
