import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oktoast/oktoast.dart' as oktoast;

AppBar mainAppBar({required String title, Widget? leftButton, Widget? rightButton, TabBar? bottomWidget}) {
  return AppBar(
    leading: leftButton,
    title: Text(
      title,
      style: TextStyle(
        color: Colors.black,
        fontSize: 34.sp,
        fontWeight: FontWeight.w600,
      ),
    ),
    bottom: bottomWidget,
    elevation: 0,
    centerTitle: true,
    backgroundColor: Colors.white,
    actions: [rightButton ?? const SizedBox.shrink()],
  );
}

showToast(String message, {time = 3}) {
  return oktoast.showToast(message, duration: Duration(seconds: time));
}
