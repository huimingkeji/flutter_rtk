class AppAssets {
  // tabbar
  static const String tabbarSelected = "assets/tabs/%s_selected.png";
  static const String tabbarUnselect = "assets/tabs/%s_unselect.png";

  // images
  static const String splashBg = "assets/images/splash_bg.png";
  static const String defaultAvatar = "assets/images/default_avatar.png";
  static const String defaultBanner = "assets/images/default_banner.png";
  static const String loginBg = "assets/images/login_bg.png";
  static const String loginLogo = "assets/images/login_logo.png";
  static const String loginLogoBg = "assets/images/login_logo_bg.png";
  static const String refreshIcon = "assets/images/refresh_icon.png";
  static const String moreTools = "assets/images/more_tools.png";
  static const String homeMessage = "assets/images/home_message.png";
  static const String homeTodo01 = "assets/images/todo01.png";
  static const String homeTodo02 = "assets/images/todo02.png";
  static const String homeTodo03 = "assets/images/todo03.png";
  static const String mineBg = "assets/images/mine_bg.png";
  static const String mineRightArrow = "assets/images/mine_right_arrow.png";
  static const String mineProject = "assets/images/mine_project.png";
  static const String minePhone = "assets/images/mine_phone.png";
  static const String mineAboutUs = "assets/images/mine_about_us.png";
  static const String mineSetting = "assets/images/mine_setting.png";
  static const String toolsEdit = "assets/images/tools_edit.png";
  static const String toolsShowAll = "assets/images/tools_show_all.png";
  static const String toolsCanRemove = "assets/images/tools_can_remove.png";
  static const String toolsSelected = "assets/images/tools_selected.png";
  static const String toolsUnselect = "assets/images/tools_unselect.png";
  static const String projectSearchIcon = "assets/images/search_icon.png";
  static const String projectFolderIcon = "assets/images/folder_icon.png";
  static const String projectClearIcon = "assets/images/clear_icon.png";
  static const String projectRightArrowIcon = "assets/images/right_arrow_icon.png";
  static const String projectDownArrowIcon = "assets/images/down_arrow_icon.png";
  static const String projectTreeIcon = "assets/images/tree_icon.png";
  static const String undoTicket = "assets/images/undo_ticket.png";
  static const String offlineIcon = "assets/images/offline_icon.png";
}
