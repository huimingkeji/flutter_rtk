class AppConfig {
  // 测试环境
  // static const String baseUrl = "https://cip-test.sinoma-ncdri.cn";

  // 正式环境
  // static const String baseUrl = "https://cip.sinoma-ncdri.cn";

  //
  static const String defaultTenantId = "000000";
  static const String clientId = "sinoma";
  static const String clientSecret = "sinoma_secret";

  // 首页待办跳转h5前缀(只有正式环境地址)
  static const String homeTodoCIPPrefixDomain = "https://cip.sinoma-ncdri.cn/#/wxGoH5";
  static const String homeTodoEPCPrefixDomain = "https://epc1.sinoma-ncdri.cn:8097/Sinoma_SSO.html";
}
