import 'package:flutter/material.dart';
import 'package:flutter_rtk/common/constants.dart';
import 'package:flutter_rtk/pages/app_page.dart';
import 'package:flutter_rtk/pages/home/message_detail_page.dart';
import 'package:flutter_rtk/pages/home/message_page.dart';
import 'package:flutter_rtk/pages/home/my_todo_page.dart';
import 'package:flutter_rtk/pages/home/project_switch.dart';
import 'package:flutter_rtk/pages/login/index.dart';
import 'package:flutter_rtk/pages/mine/project_page.dart';
import 'package:flutter_rtk/pages/mine/see_data_demo.dart';
import 'package:flutter_rtk/pages/mine/setting_page.dart';
import 'package:flutter_rtk/pages/splash/index.dart';
import 'package:flutter_rtk/pages/webview/index.dart';
import 'package:flutter_rtk/service/store_service.dart';

class AppRouters {
  static String initial = "/";
  static String splash = "/splash";
  static String login = "/login";
  static String webview = "/webview";
  static String toSwitchProjectPage = "/switchProject";
  static String toMyTodoPage = "/myTodoList";
  static String toMessagePage = "/messageList";
  static String toMessageDetailPage = "/messageDetail";
  static String toMyProjectPage = "/myProject";
  static String toSettingPage = "/setting";
  static String toSeeDataDemoPage = "/seeDataDemo";

  Map<String, WidgetBuilder> routes = {
    splash: (context) => const SplashPage(),
    initial: (context) => const AppPage(),
    login: (context) => const LoginPage(),
    toSwitchProjectPage: (context) => const ProjectSwitchPage(),
    toMyTodoPage: (context) => const MyTodoPage(),
    toMessagePage: (context) => const MessagePage(),
    toMyProjectPage: (context) => const MyProjectPage(),
    toSettingPage: (context) => const SettingPage(),
    toSeeDataDemoPage: (context) => const SeeDataDemo(),
  };

  onGenerateRoute(RouteSettings settings) {
    String name = settings.name ?? initial;
    // bool deviceIsFirstOpen = StoreService.to.getBool(AppConstants.deviceIsFirstOpen);
    // if (!deviceIsFirstOpen) {
    //   return MaterialPageRoute(settings: settings, builder: (context) => const SplashPage());
    // }
    // // 判断是否支持自动登陆
    // bool autoLogin = StoreService.to.getBool(AppConstants.autoLogin);
    // if (autoLogin) {
    //   // 如果当前时间 减去 上次登陆时间 超过7天, 则跳转登陆页面,重新登陆.
    //   int lastLoginTime = StoreService.to.getInt(AppConstants.autoLoginTime);
    //   int now = DateTime.now().millisecondsSinceEpoch;
    //   if (now - lastLoginTime > 7 * 86400000) {
    //     return MaterialPageRoute(settings: settings, builder: (context) => routes[login]!(context));
    //   }
    // }
    // 用户未登陆，则进入登陆页面; 如果下面放开，不进行每次登陆，则无法缓存用户信息。离线无法使用。。。。。
    // if (!StoreService.to.getBool(AppConstants.userIsLogin)) {
    //   return MaterialPageRoute(settings: settings, builder: (context) => routes[login]!(context));
    // }
    if (name == webview) {
      Map args = settings.arguments as Map;
      return MaterialPageRoute(settings: settings, builder: (context) => WebViewPage(title: args["title"], url: args["url"]));
    }
    if (name == toMessageDetailPage) {
      Map args = settings.arguments as Map;
      return MaterialPageRoute(settings: settings, builder: (context) => MessageDetailPage(title: args["title"], messageId: "${args["messageId"]}"));
    }
    return MaterialPageRoute(settings: settings, builder: (context) => routes[name]!(context));
  }
}
