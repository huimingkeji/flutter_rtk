extension StringExtesion on String {
  /// 驼峰转下划线
  String toSnakeCase() {
    // Replace the spaces with underscores
    String underscoreCase = replaceAll(' ', '_');
    // Split the string by non-word characters and capitalize the first letter of each word
    List<String> words = underscoreCase.split(RegExp(r'\W+'));
    String snakeCase = '';
    for (int i = 0; i < words.length; i++) {
      if (i == 0) {
        snakeCase += words[i].toLowerCase();
      } else {
        snakeCase += words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase();
      }
    }
    return snakeCase;
  }

  String toCamelCase() {
    // Split the string by underscore
    List<String> words = split('_');
    // Initialize an empty string for camel case
    String camelCase = '';
    // Concatenate the words with the first letter capitalized
    for (int i = 0; i < words.length; i++) {
      if (i == 0) {
        camelCase += words[i].toLowerCase();
      } else {
        camelCase += words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase();
      }
    }
    return camelCase;
  }
}
