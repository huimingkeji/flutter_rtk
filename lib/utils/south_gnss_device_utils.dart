import 'package:flutter/services.dart';

class SouthGnssDeviceUtils {
  static const MethodChannel _instance = MethodChannel("com.rtk.flutter_rtk");

  static start(Function callback) async {
    // 设置监听
    _instance.setMethodCallHandler((methodCall) async {
      switch (methodCall.method) {
        case 'getLocationForResult':
          callback(methodCall.arguments);
          break;
      }
    });
    if (!(await getConnectState())) {
      // await _instance.invokeMethod("connect");
      await _instance.invokeMethod("deviceSettings");
      await _instance.invokeMethod("settingPremiss");
    }
    await _instance.invokeMethod("registerListener");
  }

  static getConnectState() async {
    return await _instance.invokeMethod("getConnection");
  }

  static remove() async {
    await _instance.invokeMethod("destroyListener");
  }
}
