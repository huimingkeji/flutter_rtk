import 'package:broadcast_cdt/broadcast_cdt.dart';
import 'package:flutter/cupertino.dart';

class CDTUtils {
  static final BroadcastCdt _instance = BroadcastCdt();

  static start(Function callback) {
    _instance.startLocationReceiver().then((eventChannel) {
      if (eventChannel == null) {
        return;
      }
      callback(eventChannel);
    });
  }

  static remove() async {
    await _instance.removeLocationReceiver();
  }

  static isRun(package) async {
    try {
      return await _instance.isRunApplication(package);
    } catch (e, stack) {
      debugPrintStack(stackTrace: stack);
    }
    return false;
  }
}
