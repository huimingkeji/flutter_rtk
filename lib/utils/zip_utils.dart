import 'dart:io';

import 'package:archive/archive_io.dart';

class ZipUtils {
  static String unZip({required String zipPath, required String outputPath}) {
    if (!File(zipPath).existsSync()) {
      return "解压失败，文件不存在！";
    }
    final inputStream = InputFileStream(zipPath);
    final archive = ZipDecoder().decodeBuffer(inputStream);
    for (var file in archive.files) {
      if (file.isFile) {
        final outputStream = OutputFileStream('$outputPath/${file.name}');
        file.writeContent(outputStream);
        outputStream.close();
      }
    }
    return "";
  }
}
