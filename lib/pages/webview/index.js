function FlutterInvoker() {
    let variableNum = 0
    window.flutterBridgeEventObj = {}
    this.call = function (method, data, callBack) {
        variableNum++
        window.flutterBridgeEventObj[`callBack`] = callBack
        const postData = JSON.stringify({
            method,
            data: JSON.stringify(data),
            callback: `window.flutterBridgeEventObj.callBack`
        })
        // call flutter api
        try {
            FlutterApi2.postMessage(postData);
        } catch (_) {
        }
    }
}
window.FlutterInvoker = new FlutterInvoker();


function insertRow() {
    let data = {
                   "baseInfo": {
                       "projectId": null,
                       "userId": null,
                       "jobShop": "",
                       "foundationPitName": "123",
                       "excavateType": "small",
                       "excavateShapes": "rectangle",
                       "baseLengthA": "1",
                       "baseWidthB": "2",
                       "slopeCoefficientM": "0.30",
                       "drainageDitchWidth": "3",
                       "cushionThickness": "4"
                   }
               };
    FlutterInvoker.call("database", {
        "op":'insert',
        'business': 't_demo',
        'text': JSON.stringify(data)
    }, (data) => {
        console.log(data);
        // JSON.parse(data)['data']
        var elem = document.getElementById("id");
        elem.value = JSON.parse(data)['data'];
    })
}
function updateRow() {
    FlutterInvoker.call("database", {
        "op":'update',
        "id": document.getElementById("id").value,
        'text': 'hellod,dfafd.fda,fdasfl;dsafadfl,lfdasfd,hellod,dfafd.fda,fdasfl;dsafadfl,lfdasfd,'
    }, (data) => {
        console.log(data);
    })
}
function deleteRow() {
    var id = document.getElementById("id").value;
    console.log(id);
    FlutterInvoker.call("database", {
        "op":'delete',
        "id": id,
    }, (data) => {
        console.log(data, JSON.parse(data));
    })
}
function queryRow() {
    FlutterInvoker.call("database", {
        "op":'query',
        'where':'business = ?',
        'whereArgs':['t_demo']
    }, (data) => {
        console.log(data);
        console.log(typeof(data));
        console.log("---------")
        console.log(JSON.stringify(data));
        console.log("---------")
    })
}

function queryNew() {
    FlutterInvoker.call("database", {
        "op":'query',
        "tableName":'t_compare_table',
    }, (data) => {
        console.log(data);
    })
}