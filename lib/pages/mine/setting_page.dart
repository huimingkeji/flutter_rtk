import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rtk/common/assets.dart';
import 'package:flutter_rtk/common/common.dart';
import 'package:flutter_rtk/models/user_info_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({super.key});

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  _buildMainView() {
    return Scaffold(
      appBar: mainAppBar(title: "设置"),
      body: Consumer<UserInfoModel>(builder: (context, user, child) {
        return Center(
          child: Container(
            width: 720.w,
            height: 1125.h,
            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(15.r)),
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              _buildItem(
                label: "头像",
                height: 186.h,
                suffixWidget: Container(
                  width: 120.w,
                  height: 120.w,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(120.r)),
                  clipBehavior: Clip.antiAlias,
                  child: user.userInfo.avatar.isNotEmpty ? CachedNetworkImage(imageUrl: user.userInfo.avatar, fit: BoxFit.cover) : Image.asset(AppAssets.defaultAvatar),
                ),
              ),
              Divider(color: const Color(0xFFE3E3E3), indent: 25.w, endIndent: 25.w),
              _buildItem(label: "姓名", suffixLabel: user.userInfo.name),
              Divider(color: const Color(0xFFE3E3E3), indent: 25.w, endIndent: 25.w),
              _buildItem(label: "公司", suffixLabel: user.userInfo.orgName),
              Divider(color: const Color(0xFFE3E3E3), indent: 25.w, endIndent: 25.w),
              _buildItem(label: "部门", suffixLabel: user.userInfo.deptName),
              Divider(color: const Color(0xFFE3E3E3), indent: 25.w, endIndent: 25.w),
              _buildItem(
                label: "电子签名",
                suffixWidget: SizedBox(
                  width: 100.w,
                  height: 30.h,
                  child: FittedBox(
                    child: Row(children: [
                      Text(fillSign(user.userInfo.isSignature), style: TextStyle(fontSize: 28.sp, color: const Color(0xFF333333), fontWeight: FontWeight.normal)),
                      SizedBox(width: 10.w),
                      SizedBox(width: 20.w, height: 30.h, child: Image.asset(AppAssets.mineRightArrow)),
                    ]),
                  ),
                ),
              ),
              Divider(color: const Color(0xFFE3E3E3), indent: 25.w, endIndent: 25.w),
              user.userInfo.isSignature == 2
                  ? Container(
                      width: 170.w,
                      height: 170.w,
                      margin: EdgeInsets.only(left: 33.w, top: 30.h),
                      clipBehavior: Clip.antiAlias,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.r)),
                      child: CachedNetworkImage(imageUrl: user.userInfo.signature, fit: BoxFit.fill),
                    )
                  : const SizedBox.shrink(),
            ]),
          ),
        );
      }),
    );
  }

  _buildItem({required String label, String? suffixLabel, Widget? suffixWidget, double? height, bool rightPadding = true}) {
    height = height ?? 80.h;
    return Container(
      width: double.infinity,
      height: height,
      padding: rightPadding ? EdgeInsets.symmetric(horizontal: 33.w) : EdgeInsets.only(left: 14.w),
      child: Row(children: [
        Text(label, style: TextStyle(fontSize: 30.sp, color: const Color(0xFF333333), fontWeight: FontWeight.normal)),
        const Spacer(),
        suffixWidget ?? Text(suffixLabel!, style: TextStyle(fontSize: 30.sp, color: const Color(0xFF333333), fontWeight: FontWeight.w500)),
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildMainView();
  }

  String fillSign(int isSignature) {
    switch (isSignature) {
      case 1:
        return "未签名";
      case 2:
        return "已签名";
      case 3:
        return "驳回";
    }
    return "Unknown";
  }
}
