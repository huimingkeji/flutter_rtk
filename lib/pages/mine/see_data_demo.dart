import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rtk/common/common.dart';
import 'package:flutter_rtk/service/database_service.dart';

class SeeDataDemo extends StatefulWidget {
  const SeeDataDemo({super.key});

  @override
  State<SeeDataDemo> createState() => _SeeDataDemoState();
}

class _SeeDataDemoState extends State<SeeDataDemo> {
  List<Map<String, Object?>> data = [];

  @override
  void initState() {
    super.initState();
    // 获取所有表
    _fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainAppBar(title: "数据查看"),
      body: ListView.builder(
        itemBuilder: (context, idx) {
          Map<String, Object?> item = data[idx];
          return ListTile(
            title: const Text("t_business_sync"),
            subtitle: Text(jsonEncode(item)),
          );
        },
        itemCount: data.length,
      ),
    );
  }

  _fetchData() async {
    List<Map<String, Object?>> items = await DatabaseService.to.find(tableName: "t_business_sync");
    if (items.isNotEmpty) {
      for (Map<String, Object?> value in items) {
        data.add(value);
      }
    }
    setState(() {});
  }
}
