import 'package:flutter/material.dart';
import 'package:flutter_rtk/common/assets.dart';
import 'package:flutter_rtk/common/constants.dart';
import 'package:flutter_rtk/router/index.dart';
import 'package:flutter_rtk/service/store_service.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(children: [
        // 上层
        Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: 72.w),
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            const Spacer(),
            SizedBox(
                width: double.infinity,
                height: MediaQuery.of(context).size.height - 220.h,
                child: Image.asset(AppAssets.splashBg)),
            const Spacer(),
            SizedBox(
              width: double.infinity,
              height: 90.h,
              child: TextButton(
                style: ButtonStyle(
                  splashFactory: NoSplash.splashFactory,
                  backgroundColor: MaterialStateProperty.all(AppConstants.brandColor),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(610.r))),
                  padding: MaterialStateProperty.all(EdgeInsets.zero),
                  elevation: MaterialStateProperty.all(0),
                ),
                onPressed: () async {
                  await StoreService().setBool(AppConstants.deviceIsFirstOpen, true);
                  await StoreService.to.setString(AppConstants.baseUrl, "http://58.218.37.130:38080");
                  // ignore: use_build_context_synchronously
                  Navigator.of(context).pushNamedAndRemoveUntil(AppRouters.login, (router) => false);
                },
                child:
                    Text("立即体验", style: TextStyle(fontSize: 36.sp, color: Colors.white, fontWeight: FontWeight.w500)),
              ),
            ),
            // 验证码登陆
            SizedBox(height: 100.h)
          ]),
        ),
      ]),
    );
  }
}
