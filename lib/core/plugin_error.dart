class PluginError extends Error {
  final String error;

  PluginError(this.error);
}
