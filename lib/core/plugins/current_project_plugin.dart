import 'package:flutter/cupertino.dart';
import 'package:flutter_rtk/core/flutter_bridge.dart';
import 'package:flutter_rtk/core/plugin_execute.dart';
import 'package:flutter_rtk/entities/current_project_entity.dart';
import 'package:flutter_rtk/models/app_model.dart';
import 'package:provider/provider.dart';

class CurrentProjectPlugin extends PluginExecute {
  @override
  doExecute(BuildContext context, Result result) async {
    // List<Map<String, Object?>> projects = await DatabaseService.to.find(tableName: "t_project", where: "is_current=?", whereArgs: [1]);
    // if (projects.isNotEmpty) {
    //   var item = projects[0];
    //   var entity = ProjectEntity("${item['project_name']}", "${item['project_code']}", "${item['project_id']}", "${item['dept_code']}", "${item['dept_name']}", "{${item['project_type']}", "${item['parent_id']}", [], [], "${item['merge_name']}");
    //   CurrentProjectEntity offlineCurrentProjectEntity = CurrentProjectEntity(entity.projectId, "--", entity.deptName, "--", "--", "--", 0, 0, "", entity.projectCode, entity.projectId, entity.deptCode, "", entity.mergeName);
    //   result.callback({'code': 0, 'data': offlineCurrentProjectEntity});
    // }
    CurrentProjectEntity? currentProjectInfo = context.read<AppModel>().currentProjectInfo;
    if (currentProjectInfo == null) {
      result.callback({'code': -1, 'message': "无项目信息"});
      return;
    }
    result.callback({'code': 0, 'data': currentProjectInfo});
  }
}
