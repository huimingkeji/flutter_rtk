import 'package:flutter/material.dart';
import 'package:flutter_rtk/common/common.dart';
import 'package:flutter_rtk/core/flutter_bridge.dart';
import 'package:flutter_rtk/core/plugin_execute.dart';

class ShowToastPlugin extends PluginExecute {
  @override
  doExecute(BuildContext context, Result result) async {
    await showToast(result.data);
    result.callback(true);
  }
}
