import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_rtk/core/flutter_bridge.dart';
import 'package:flutter_rtk/core/plugin_execute.dart';
import 'package:flutter_rtk/utils/image_picker_utils.dart';

class UploadImagePlugin extends PluginExecute {
  @override
  doExecute(BuildContext context, Result result) {
    bool isCanUpload = true;
    var params = jsonDecode(result.data);
    if (params != null) {
      bool? isUploadParam = params['isUpload'];
      if (isUploadParam != null) {
        isCanUpload = isUploadParam;
      }
    }
    String business = params['business'] ?? "RTK"; // 目前只有RTK，理论上这个需要H5传过来
    ImagePickerUtils().openSelectPicture(context, (List results) {
      result.callback(jsonEncode(results));
      EasyLoading.dismiss();
    }, business: business, isUpload: isCanUpload);
  }
}
