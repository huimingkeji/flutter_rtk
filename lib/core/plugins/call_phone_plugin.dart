import 'package:flutter/material.dart';
import 'package:flutter_rtk/core/flutter_bridge.dart';
import 'package:flutter_rtk/core/plugin_execute.dart';
import 'package:url_launcher/url_launcher.dart';

class CallPhonePlugin extends PluginExecute {
  @override
  doExecute(BuildContext context, Result result) async {
    if (result.data.isEmpty) {
      result.callback(false);
      return;
    }
    result.callback(await launchUrl(Uri(scheme: "tel", path: result.data)));
  }
}
