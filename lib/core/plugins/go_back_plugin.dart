import 'package:flutter/material.dart';
import 'package:flutter_rtk/core/flutter_bridge.dart';
import 'package:flutter_rtk/core/plugin_execute.dart';

class GoBackPlugin extends PluginExecute {
  @override
  doExecute(BuildContext context, Result result) {
    Navigator.pop(context);
    result.callback(true);
  }
}
