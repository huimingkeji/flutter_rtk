import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rtk/common/common.dart';
import 'package:flutter_rtk/core/flutter_bridge.dart';
import 'package:flutter_rtk/core/plugin_execute.dart';
import 'package:flutter_rtk/utils/cdt_utils.dart';
import 'package:flutter_rtk/utils/south_gnss_device_utils.dart';

class LocationReceiverWatchPlugin extends PluginExecute {
  @override
  doExecute(BuildContext context, Result result) {
    var params = jsonDecode(result.data);
    if (params == null || params.isEmpty) {
      showToast("开启位置接收, 缺失必要参数");
      result.callback({});
      return;
    }
    int type = params['type'] ?? 0;
    if (type == 0) {
      // 测地通
      CDTUtils.start((EventChannel eventChannel) {
        eventChannel.receiveBroadcastStream().listen((data) {
          result.callback(data);
        });
      });
    } else if (type == 1) {
      // 南方
      SouthGnssDeviceUtils.start((data) {
        result.callback(data);
      });
    }
  }
}
