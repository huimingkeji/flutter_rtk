import 'package:flutter/cupertino.dart';
import 'package:flutter_rtk/core/plugin_execute.dart';
import 'package:flutter_rtk/service/event_bus_service.dart';

import '../flutter_bridge.dart';

class CheckCDTPlugin extends PluginExecute {
  @override
  doExecute(BuildContext context, Result result) {
    result.callback(EventBusService.isCanWatchCDT);
    return Future(() => true);
  }
}
