import 'package:flutter/cupertino.dart';
import 'package:flutter_rtk/core/flutter_bridge.dart';
import 'package:flutter_rtk/core/plugin_execute.dart';
import 'package:flutter_rtk/service/database_service.dart';

class GetUserInfoPlugin extends PluginExecute {
  @override
  doExecute(BuildContext context, Result result) async {
    List<Map<String, Object?>> users = await DatabaseService.to.find(tableName: "t_user", where: "id = ?", whereArgs: [1]);
    if (users.isEmpty) {
      result.callback({'code': -1, 'message': "无用户信息"});
      return;
    }
    result.callback({'code': 0, 'data': users[0]});
  }
}
