import 'package:flutter/material.dart';
import 'package:flutter_rtk/common/constants.dart';
import 'package:flutter_rtk/core/flutter_bridge.dart';
import 'package:flutter_rtk/core/plugin_execute.dart';
import 'package:flutter_rtk/service/store_service.dart';

class GetTokenPlugin extends PluginExecute {
  @override
  doExecute(BuildContext context, Result result) {
    return StoreService.to.getString(AppConstants.token);
  }

  @override
  bool isSync() => true;
}
