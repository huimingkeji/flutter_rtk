import 'package:flutter/material.dart';
import 'package:flutter_rtk/common/common.dart';
import 'package:flutter_rtk/core/flutter_bridge.dart';

abstract class PluginExecute {
  static final Map _pluginCache = {};
  static final Map _pluginJsonCache = {};

  bool isSync() => false;

  doExecute(BuildContext context, Result result);

  static registerWith(String method, PluginExecute pe) {
    _pluginCache[method] = pe;
  }

  static registerWithJson(String method, PluginExecute pe) {
    _pluginJsonCache[method] = pe;
  }

  static PluginExecute of({required String method}) {
    return _pluginCache[method] ?? ErrorMethodPlugin();
  }

  static PluginExecute ofJson({required String method}) {
    return _pluginJsonCache[method] ?? ErrorMethodPlugin();
  }
}

class ErrorMethodPlugin extends PluginExecute {
  @override
  doExecute(BuildContext context, Result result) {
    showToast("错误的插件调用");
  }
}
