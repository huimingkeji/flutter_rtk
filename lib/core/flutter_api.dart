import 'package:flutter/material.dart';
import 'package:flutter_rtk/common/common.dart';
import 'package:flutter_rtk/core/flutter_bridge.dart';
import 'package:flutter_rtk/core/plugin_error.dart';
import 'package:flutter_rtk/core/plugin_execute.dart';
import 'package:flutter_rtk/core/plugins/call_phone_plugin.dart';
import 'package:flutter_rtk/core/plugins/check_cdt_app_plugin.dart';
import 'package:flutter_rtk/core/plugins/current_project_plugin.dart';
import 'package:flutter_rtk/core/plugins/current_user_info_plugin.dart';
import 'package:flutter_rtk/core/plugins/database_operate_plugin.dart';
import 'package:flutter_rtk/core/plugins/get_token_plugin.dart';
import 'package:flutter_rtk/core/plugins/go_back_plugin.dart';
import 'package:flutter_rtk/core/plugins/location_receiver_remove_plugin.dart';
import 'package:flutter_rtk/core/plugins/location_receiver_watch_plugin.dart';
import 'package:flutter_rtk/core/plugins/scan_qrcode_plugin.dart';
import 'package:flutter_rtk/core/plugins/show_toast_plugin.dart';
import 'package:flutter_rtk/core/plugins/upload_image_plugin.dart';

class FlutterApi {
  static executeRespJson(BuildContext context, flutterWebViewPlugin, String jsonStr) {
    FlutterBridge bridge = FlutterBridge(flutterWebViewPlugin);
    Result result = bridge.dispatch(jsonStr, isJson: true);
    execute(context, result, true);
  }

  // 向H5调用接口
  static executeRespString(BuildContext context, flutterWebViewPlugin, String jsonStr) {
    FlutterBridge bridge = FlutterBridge(flutterWebViewPlugin);
    Result result = bridge.dispatch(jsonStr, isJson: false);
    execute(context, result, false);
  }

  static execute(BuildContext context, Result result, bool isJson) {
    try {
      PluginExecute plugin;
      if (isJson) {
        plugin = PluginExecute.ofJson(method: result.method);
      } else {
        plugin = PluginExecute.of(method: result.method);
      }
      if (plugin.isSync()) {
        return plugin.doExecute(context, result);
      }
      plugin.doExecute(context, result);
    } on PluginError catch (e) {
      showToast("$e");
      result.callback({'code': -1, 'message': e});
    } catch (e) {
      showToast("$e");
      result.callback({'code': -9999, 'message': '未知错误.'});
    }
  }

  static registerPlugins() {
    // reg string resp
    PluginExecute.registerWith("callPhone", CallPhonePlugin());
    PluginExecute.registerWith("isRunCDTApp", CheckCDTPlugin());
    PluginExecute.registerWith("getToken", GetTokenPlugin());
    PluginExecute.registerWith("goBack", GoBackPlugin());
    PluginExecute.registerWith("startLocationReceiver", LocationReceiverWatchPlugin());
    PluginExecute.registerWith("removeLocationReceiver", LocationReceiverRemovePlugin());
    PluginExecute.registerWith("scanQRCode", ScanQRCodePlugin());
    PluginExecute.registerWith("showToast", ShowToastPlugin());
    PluginExecute.registerWith("uploadImage", UploadImagePlugin());
    // reg json resp
    PluginExecute.registerWithJson("database", DatabaseOperatePlugin());
    PluginExecute.registerWithJson("getCurrentProject", CurrentProjectPlugin());
    PluginExecute.registerWithJson("getUserInfo", GetUserInfoPlugin());
  }
}
