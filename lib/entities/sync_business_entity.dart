class SyncBusinessEntity {
  SyncBusinessEntity({
    required this.id,
    required this.business,
    required this.text,
    required this.images,
    required this.createTime,
    this.updateTime = -1,
    this.syncTime = -1,
    this.isSynchronized = 0,
  });

  SyncBusinessEntity.fromJson(dynamic json) {
    id = json['id'];
    business = json['business'];
    text = json['text'];
    images = json['images'];
    createTime = json['create_time'];
    updateTime = json['update_time'] ?? -1;
    syncTime = json['sync_time'] ?? -1;
    isSynchronized = json['is_synchronized'] ?? 0;
  }

  late String id;
  late String business;
  late String text;
  late String images;
  late int createTime;
  late int updateTime;
  late int syncTime;
  late int isSynchronized;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['business'] = business;
    map['text'] = text;
    map['images'] = images;
    map['create_time'] = createTime;
    map['update_time'] = updateTime;
    map['sync_time'] = syncTime;
    map['is_synchronized'] = isSynchronized;
    return map;
  }
}
