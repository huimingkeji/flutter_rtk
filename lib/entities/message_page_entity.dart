import 'package:json_annotation/json_annotation.dart';

part 'message_page_entity.g.dart';

@JsonSerializable()
class MessagePageEntity extends Object {
  @JsonKey(name: 'records')
  List<MessageInfo> records;

  @JsonKey(name: 'total')
  int total;

  @JsonKey(name: 'size')
  int size;

  @JsonKey(name: 'current')
  int current;

  @JsonKey(name: 'orders')
  List<dynamic> orders;

  @JsonKey(name: 'optimizeCountSql')
  bool optimizeCountSql;

  @JsonKey(name: 'searchCount')
  bool searchCount;

  @JsonKey(name: 'countId')
  String countId;

  @JsonKey(name: 'maxLimit')
  int maxLimit;

  @JsonKey(name: 'pages')
  int pages;

  MessagePageEntity(
    this.records,
    this.total,
    this.size,
    this.current,
    this.orders,
    this.optimizeCountSql,
    this.searchCount,
    this.countId,
    this.maxLimit,
    this.pages,
  );

  factory MessagePageEntity.fromJson(Map<String, dynamic> srcJson) => _$MessagePageEntityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$MessagePageEntityToJson(this);
}

@JsonSerializable()
class MessageInfo extends Object {
  @JsonKey(name: 'id')
  String id;

  @JsonKey(name: 'createUser')
  String createUser;

  @JsonKey(name: 'createDept')
  dynamic createDept;

  @JsonKey(name: 'createTime')
  String createTime;

  @JsonKey(name: 'updateUser')
  String updateUser;

  @JsonKey(name: 'updateTime')
  String updateTime;

  @JsonKey(name: 'status')
  int? status;

  @JsonKey(name: 'isDeleted')
  int? isDeleted;

  @JsonKey(name: 'tenantId')
  String tenantId;

  @JsonKey(name: 'msgId')
  dynamic msgId;

  @JsonKey(name: 'detailCode')
  String detailCode;

  @JsonKey(name: 'channelId')
  String channelId;

  @JsonKey(name: 'channelType')
  String channelType;

  @JsonKey(name: 'channelName')
  String channelName;

  @JsonKey(name: 'sendAccount')
  String sendAccount;

  @JsonKey(name: 'userId')
  String userId;

  @JsonKey(name: 'receiveAccount')
  String receiveAccount;

  @JsonKey(name: 'receiveName')
  String receiveName;

  @JsonKey(name: 'msgTile')
  String msgTile;

  @JsonKey(name: 'msgContent')
  String msgContent;

  @JsonKey(name: 'isRead')
  int isRead;

  @JsonKey(name: 'pcUrl')
  String pcUrl;

  @JsonKey(name: 'appUrl')
  String appUrl;

  @JsonKey(name: 'sourceCode')
  String sourceCode;

  @JsonKey(name: 'sendStatus')
  String sendStatus;

  @JsonKey(name: 'templateCode')
  String templateCode;

  @JsonKey(name: 'startDate')
  String startDate;

  @JsonKey(name: 'endDate')
  String endDate;

  @JsonKey(name: 'params')
  String params;

  @JsonKey(name: 'templateId')
  String templateId;

  MessageInfo(
    this.id,
    this.createUser,
    this.createDept,
    this.createTime,
    this.updateUser,
    this.updateTime,
    this.status,
    this.isDeleted,
    this.tenantId,
    this.msgId,
    this.detailCode,
    this.channelId,
    this.channelType,
    this.channelName,
    this.sendAccount,
    this.userId,
    this.receiveAccount,
    this.receiveName,
    this.msgTile,
    this.msgContent,
    this.isRead,
    this.pcUrl,
    this.appUrl,
    this.sourceCode,
    this.sendStatus,
    this.templateCode,
    this.startDate,
    this.endDate,
    this.params,
    this.templateId,
  );

  factory MessageInfo.fromJson(Map<String, dynamic> srcJson) => _$MessageInfoFromJson(srcJson);

  Map<String, dynamic> toJson() => _$MessageInfoToJson(this);
}
