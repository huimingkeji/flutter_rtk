// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tools_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ToolsEntity _$ToolsEntityFromJson(Map<String, dynamic> json) => ToolsEntity(
      json['id'],
      json['parentId'],
      json['code'],
      json['name'],
      json['alias'],
      json['path'],
      json['source'],
      json['sort'],
      json['category'],
      json['action'],
      json['isOpen'],
      json['status'],
      json['remark'],
      json['platform'],
      json['color'],
      json['filePath'],
      json['isDeleted'],
      json['isOnline'],
      json['sourceId'],
      json['typeSetting'],
      json['defaultLanguage'],
      json['systemLanguage'],
      (json['children'] ?? []),
      json['hasChildren'],
      json['parentName'],
      json['categoryName'],
      json['actionName'],
      json['isOpenName'],
    );

Map<String, dynamic> _$ToolsEntityToJson(ToolsEntity instance) => <String, dynamic>{
      'id': instance.id,
      'parentId': instance.parentId,
      'code': instance.code,
      'name': instance.name,
      'alias': instance.alias,
      'path': instance.path,
      'source': instance.source,
      'sort': instance.sort,
      'category': instance.category,
      'action': instance.action,
      'isOpen': instance.isOpen,
      'status': instance.status,
      'remark': instance.remark,
      'platform': instance.platform,
      'color': instance.color,
      'filePath': instance.filePath,
      'isDeleted': instance.isDeleted,
      'isOnline': instance.isOnline,
      'sourceId': instance.sourceId,
      'typeSetting': instance.typeSetting,
      'defaultLanguage': instance.defaultLanguage,
      'systemLanguage': instance.systemLanguage,
      'children': instance.children,
      'hasChildren': instance.hasChildren,
      'parentName': instance.parentName,
      'categoryName': instance.categoryName,
      'actionName': instance.actionName,
      'isOpenName': instance.isOpenName,
    };
