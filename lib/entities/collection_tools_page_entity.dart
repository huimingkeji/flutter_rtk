import 'package:json_annotation/json_annotation.dart';

part 'collection_tools_page_entity.g.dart';

@JsonSerializable()
class CollectionToolsPageEntity extends Object {
  @JsonKey(name: 'records')
  List<CollectionToolsItem> records;

  @JsonKey(name: 'total')
  int total;

  @JsonKey(name: 'size')
  int size;

  @JsonKey(name: 'current')
  int current;

  @JsonKey(name: 'orders')
  List<dynamic> orders;

  @JsonKey(name: 'optimizeCountSql')
  bool optimizeCountSql;

  @JsonKey(name: 'searchCount')
  bool searchCount;

  @JsonKey(name: 'countId')
  String countId;

  @JsonKey(name: 'maxLimit')
  int maxLimit;

  @JsonKey(name: 'pages')
  int pages;

  CollectionToolsPageEntity(
    this.records,
    this.total,
    this.size,
    this.current,
    this.orders,
    this.optimizeCountSql,
    this.searchCount,
    this.countId,
    this.maxLimit,
    this.pages,
  );

  factory CollectionToolsPageEntity.fromJson(Map<String, dynamic> srcJson) => _$CollectionToolsPageEntityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CollectionToolsPageEntityToJson(this);
}

@JsonSerializable()
class CollectionToolsItem extends Object {
  @JsonKey(name: 'id')
  String id;

  @JsonKey(name: 'createUser')
  String createUser;

  @JsonKey(name: 'createDept')
  String createDept;

  @JsonKey(name: 'createTime')
  String createTime;

  @JsonKey(name: 'updateUser')
  String updateUser;

  @JsonKey(name: 'updateTime')
  String updateTime;

  @JsonKey(name: 'status')
  int status;

  @JsonKey(name: 'isDeleted')
  int isDeleted;

  @JsonKey(name: 'tenantId')
  String tenantId;

  @JsonKey(name: 'menuId')
  int menuId;

  @JsonKey(name: 'menuPath')
  String menuPath;

  @JsonKey(name: 'menuSource')
  String menuSource;

  @JsonKey(name: 'menuNameZh')
  String menuNameZh;

  @JsonKey(name: 'menuNameEn')
  String menuNameEn;

  @JsonKey(name: 'sort')
  int sort;

  @JsonKey(name: 'userId')
  String userId;

  @JsonKey(name: 'filePath')
  String filePath;

  @JsonKey(name: 'path')
  String path;

  @JsonKey(name: 'projectId')
  String projectId;

  @JsonKey(name: 'projectCode')
  String projectCode;

  CollectionToolsItem(
    this.id,
    this.createUser,
    this.createDept,
    this.createTime,
    this.updateUser,
    this.updateTime,
    this.status,
    this.isDeleted,
    this.tenantId,
    this.menuId,
    this.menuPath,
    this.menuSource,
    this.menuNameZh,
    this.menuNameEn,
    this.sort,
    this.userId,
    this.filePath,
    this.path,
    this.projectId,
    this.projectCode,
  );

  factory CollectionToolsItem.fromJson(Map<String, dynamic> srcJson) => _$CollectionToolsItemFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CollectionToolsItemToJson(this);
}
