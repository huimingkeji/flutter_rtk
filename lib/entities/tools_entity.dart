import 'package:json_annotation/json_annotation.dart';

part 'tools_entity.g.dart';

@JsonSerializable()
class ToolsEntity extends Object {
  @JsonKey(name: 'id')
  String id;

  @JsonKey(name: 'parentId')
  String parentId;

  @JsonKey(name: 'code')
  String code;

  @JsonKey(name: 'name')
  String name;

  @JsonKey(name: 'alias')
  String alias;

  @JsonKey(name: 'path')
  String path;

  @JsonKey(name: 'source')
  String source;

  @JsonKey(name: 'sort')
  int sort;

  @JsonKey(name: 'category')
  int category;

  @JsonKey(name: 'action')
  int action;

  @JsonKey(name: 'isOpen')
  int isOpen;

  @JsonKey(name: 'status')
  int status;

  @JsonKey(name: 'remark')
  String remark;

  @JsonKey(name: 'platform')
  dynamic platform;

  @JsonKey(name: 'color')
  String color;

  @JsonKey(name: 'filePath')
  String filePath;

  @JsonKey(name: 'isDeleted')
  int isDeleted;

  @JsonKey(name: 'isOnline')
  String isOnline;

  @JsonKey(name: 'sourceId')
  String? sourceId;

  @JsonKey(name: 'typeSetting')
  int? typeSetting;

  @JsonKey(name: 'defaultLanguage')
  String defaultLanguage;

  @JsonKey(name: 'systemLanguage')
  String systemLanguage;

  @JsonKey(name: 'children')
  List<dynamic> children;

  @JsonKey(name: 'hasChildren')
  bool hasChildren;

  @JsonKey(name: 'parentName')
  String parentName;

  @JsonKey(name: 'categoryName')
  String categoryName;

  @JsonKey(name: 'actionName')
  String actionName;

  @JsonKey(name: 'isOpenName')
  String isOpenName;

  ToolsEntity(
    this.id,
    this.parentId,
    this.code,
    this.name,
    this.alias,
    this.path,
    this.source,
    this.sort,
    this.category,
    this.action,
    this.isOpen,
    this.status,
    this.remark,
    this.platform,
    this.color,
    this.filePath,
    this.isDeleted,
    this.isOnline,
    this.sourceId,
    this.typeSetting,
    this.defaultLanguage,
    this.systemLanguage,
    this.children,
    this.hasChildren,
    this.parentName,
    this.categoryName,
    this.actionName,
    this.isOpenName,
  );

  factory ToolsEntity.fromJson(Map<String, dynamic> srcJson) => _$ToolsEntityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ToolsEntityToJson(this);
}
