// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProjectEntity _$ProjectEntityFromJson(Map<String, dynamic> json) =>
    ProjectEntity(
      json['projectName'] as String,
      json['projectCode'] as String,
      json['projectId'] as String,
      json['deptCode'] as String,
      json['deptName'] as String,
      json['projectType'] as String,
      json['parentId'] as String,
      json['child'] as List<dynamic>,
      json['children'] as List<dynamic>,
      json['mergeName'] as String,
    );

Map<String, dynamic> _$ProjectEntityToJson(ProjectEntity instance) =>
    <String, dynamic>{
      'projectName': instance.projectName,
      'projectCode': instance.projectCode,
      'projectId': instance.projectId,
      'deptCode': instance.deptCode,
      'deptName': instance.deptName,
      'projectType': instance.projectType,
      'parentId': instance.parentId,
      'child': instance.child,
      'children': instance.children,
      'mergeName': instance.mergeName,
    };
