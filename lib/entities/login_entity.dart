import 'package:json_annotation/json_annotation.dart';

part 'login_entity.g.dart';

@JsonSerializable()
class LoginEntity extends Object {
  @JsonKey(name: 'error')
  String? error;

  @JsonKey(name: 'error_description')
  String? errorDescription;

  @JsonKey(name: 'access_token')
  String? accessToken;

  @JsonKey(name: 'token_type')
  String? tokenType;

  @JsonKey(name: 'refresh_token')
  String? refreshToken;

  @JsonKey(name: 'expires_in')
  int? expiresIn;

  @JsonKey(name: 'scope')
  String? scope;

  @JsonKey(name: 'tenant_id')
  String? tenantId;

  @JsonKey(name: 'weak_password')
  String? weakPassword;

  @JsonKey(name: 'user_name')
  String? userName;

  @JsonKey(name: 'real_name')
  String? realName;

  @JsonKey(name: 'avatar')
  String? avatar;

  @JsonKey(name: 'client_id')
  String? clientId;

  @JsonKey(name: 'first_login')
  String? firstLogin;

  @JsonKey(name: 'role_name')
  String? roleName;

  @JsonKey(name: 'license')
  String? license;

  @JsonKey(name: 'post_id')
  String? postId;

  @JsonKey(name: 'user_id')
  String? userId;

  @JsonKey(name: 'role_id')
  String? roleId;

  @JsonKey(name: 'nick_name')
  String? nickName;

  @JsonKey(name: 'oauth_id')
  String? oauthId;

  @JsonKey(name: 'detail')
  Detail? detail;

  @JsonKey(name: 'dept_id')
  String? deptId;

  @JsonKey(name: 'account')
  String? account;

  @JsonKey(name: 'jti')
  String? jti;

  LoginEntity(
    this.error,
    this.errorDescription,
    this.accessToken,
    this.tokenType,
    this.refreshToken,
    this.expiresIn,
    this.scope,
    this.tenantId,
    this.weakPassword,
    this.userName,
    this.realName,
    this.avatar,
    this.clientId,
    this.firstLogin,
    this.roleName,
    this.license,
    this.postId,
    this.userId,
    this.roleId,
    this.nickName,
    this.oauthId,
    this.detail,
    this.deptId,
    this.account,
    this.jti,
  );

  factory LoginEntity.fromJson(Map<String, dynamic> srcJson) => _$LoginEntityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$LoginEntityToJson(this);
}

@JsonSerializable()
class Detail extends Object {
  @JsonKey(name: 'type')
  String type;

  Detail(
    this.type,
  );

  factory Detail.fromJson(Map<String, dynamic> srcJson) => _$DetailFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DetailToJson(this);
}
