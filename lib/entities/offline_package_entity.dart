class OfflinePackageEntity {
  int id;

  int packageType;

  int platformType;

  String applyType;

  String versonNumber;

  String packagePath;

  String createTime;

  String createUser;

  String updateTime;

  String updateUser;
  int isDeleted;

  OfflinePackageEntity(
    this.id,
    this.packageType,
    this.platformType,
    this.applyType,
    this.versonNumber,
    this.packagePath,
    this.createTime,
    this.createUser,
    this.updateTime,
    this.updateUser,
    this.isDeleted,
  );

  factory OfflinePackageEntity.fromJson(Map<String, dynamic> json) => OfflinePackageEntity(
        json['id'],
        json['packageType'],
        json['platformType'],
        json['applyType'],
        json['versonNumber'],
        json['packagePath'],
        json['createTime'],
        json['createUser'],
        json['updateTime'],
        json['updateUser'],
        json['isDeleted'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': id,
        'packageType': packageType,
        'platformType': platformType,
        'applyType': applyType,
        'versonNumber': versonNumber,
        'packagePath': packagePath,
        'createTime': createTime,
        'createUser': createUser,
        'updateTime': updateTime,
        'updateUser': updateUser,
        'isDeleted': isDeleted
      };
}
