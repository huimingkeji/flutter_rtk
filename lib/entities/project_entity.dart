import 'package:json_annotation/json_annotation.dart';

part 'project_entity.g.dart';

@JsonSerializable()
class ProjectEntity extends Object {
  @JsonKey(name: 'projectName')
  String projectName;

  @JsonKey(name: 'projectCode')
  String projectCode;

  @JsonKey(name: 'projectId')
  String projectId;

  @JsonKey(name: 'deptCode')
  String deptCode;

  @JsonKey(name: 'deptName')
  String deptName;

  @JsonKey(name: 'projectType')
  String projectType;

  @JsonKey(name: 'parentId')
  String parentId;

  @JsonKey(name: 'child')
  List<dynamic> child;

  @JsonKey(name: 'children')
  List<dynamic> children;

  @JsonKey(name: 'mergeName')
  String mergeName;

  ProjectEntity(
    this.projectName,
    this.projectCode,
    this.projectId,
    this.deptCode,
    this.deptName,
    this.projectType,
    this.parentId,
    this.child,
    this.children,
    this.mergeName,
  );

  factory ProjectEntity.fromJson(Map<String, dynamic> srcJson) => _$ProjectEntityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ProjectEntityToJson(this);

  ProjectEntity copyWith() {
    if (children.isNotEmpty) {
      List<dynamic> newChildren = [];
      for (var c in children) {
        newChildren.add(c);
      }
      children = newChildren;
    }
    return ProjectEntity(projectName, projectCode, projectId, deptCode, deptName, projectType, parentId, child, children, mergeName);
  }
}
