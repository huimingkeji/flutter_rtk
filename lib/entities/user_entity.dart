import 'package:json_annotation/json_annotation.dart';

part 'user_entity.g.dart';

@JsonSerializable()
class UserEntity extends Object {
  @JsonKey(name: 'id')
  String id;

  @JsonKey(name: 'createUser')
  String createUser;

  @JsonKey(name: 'createDept')
  String createDept;

  @JsonKey(name: 'createTime')
  String createTime;

  @JsonKey(name: 'updateUser')
  String updateUser;

  @JsonKey(name: 'updateTime')
  String updateTime;

  @JsonKey(name: 'status')
  int status;

  @JsonKey(name: 'isDeleted')
  int isDeleted;

  @JsonKey(name: 'tenantId')
  String tenantId;

  @JsonKey(name: 'code')
  String code;

  @JsonKey(name: 'userType')
  int userType;

  @JsonKey(name: 'account')
  String account;

  @JsonKey(name: 'name')
  String name;

  @JsonKey(name: 'realName')
  String realName;

  @JsonKey(name: 'avatar')
  String avatar;

  @JsonKey(name: 'email')
  String email;

  @JsonKey(name: 'phone')
  String phone;

  @JsonKey(name: 'birthday')
  String birthday;

  @JsonKey(name: 'sex')
  int sex;

  @JsonKey(name: 'roleId')
  String roleId;

  @JsonKey(name: 'deptId')
  String deptId;

  @JsonKey(name: 'postId')
  String postId;

  @JsonKey(name: 'gestureUnlock')
  String gestureUnlock;

  @JsonKey(name: 'passwordUnlock')
  String passwordUnlock;

  @JsonKey(name: 'firstLogin')
  String firstLogin;

  @JsonKey(name: 'weixinId')
  String weixinId;

  @JsonKey(name: 'isEnabled')
  int isEnabled;

  @JsonKey(name: 'connectionId')
  String connectionId;

  @JsonKey(name: 'identityCard')
  String identityCard;

  @JsonKey(name: 'userSource')
  int userSource;

  @JsonKey(name: 'signature')
  String signature;

  @JsonKey(name: 'isSignature')
  int isSignature;

  @JsonKey(name: 'deptName')
  String deptName;

  @JsonKey(name: 'orgName')
  String orgName;

  @JsonKey(name: 'orgId')
  String orgId;

  @JsonKey(name: 'tenantType')
  String tenantType;

  @JsonKey(name: 'tenantName')
  String tenantName;

  @JsonKey(name: 'userTypeName')
  String userTypeName;

  @JsonKey(name: 'roleName')
  String roleName;

  @JsonKey(name: 'roleAlias')
  String roleAlias;

  @JsonKey(name: 'postName')
  String postName;

  @JsonKey(name: 'sexName')
  String sexName;

  @JsonKey(name: 'userExt')
  String userExt;

  @JsonKey(name: 'webSocket')
  WebSocket webSocket;

  UserEntity(
    this.id,
    this.createUser,
    this.createDept,
    this.createTime,
    this.updateUser,
    this.updateTime,
    this.status,
    this.isDeleted,
    this.tenantId,
    this.code,
    this.userType,
    this.account,
    this.name,
    this.realName,
    this.avatar,
    this.email,
    this.phone,
    this.birthday,
    this.sex,
    this.roleId,
    this.deptId,
    this.postId,
    this.gestureUnlock,
    this.passwordUnlock,
    this.firstLogin,
    this.weixinId,
    this.isEnabled,
    this.connectionId,
    this.identityCard,
    this.userSource,
    this.signature,
    this.isSignature,
    this.deptName,
    this.orgName,
    this.orgId,
    this.tenantType,
    this.tenantName,
    this.userTypeName,
    this.roleName,
    this.roleAlias,
    this.postName,
    this.sexName,
    this.userExt,
    this.webSocket,
  );

  factory UserEntity.fromJson(Map<String, dynamic> srcJson) => _$UserEntityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$UserEntityToJson(this);
}

@JsonSerializable()
class WebSocket extends Object {
  WebSocket();

  factory WebSocket.fromJson(Map<String, dynamic> srcJson) => _$WebSocketFromJson(srcJson);

  Map<String, dynamic> toJson() => _$WebSocketToJson(this);
}
