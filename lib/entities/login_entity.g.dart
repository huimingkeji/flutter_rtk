// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginEntity _$LoginEntityFromJson(Map<String, dynamic> json) => LoginEntity(
      json['error'],
      json['error_description'],
      json['access_token'],
      json['token_type'],
      json['refresh_token'],
      json['expires_in'],
      json['scope'],
      json['tenant_id'],
      json['weak_password'],
      json['user_name'],
      json['real_name'],
      json['avatar'],
      json['client_id'],
      json['first_login'],
      json['role_name'],
      json['license'],
      json['post_id'],
      json['user_id'],
      json['role_id'],
      json['nick_name'],
      json['oauth_id'],
      json['detail'] != null ? Detail.fromJson(json['detail'] as Map<String, dynamic>) : null,
      json['dept_id'],
      json['account'],
      json['jti'],
    );

Map<String, dynamic> _$LoginEntityToJson(LoginEntity instance) => <String, dynamic>{
      'access_token': instance.accessToken,
      'token_type': instance.tokenType,
      'refresh_token': instance.refreshToken,
      'expires_in': instance.expiresIn,
      'scope': instance.scope,
      'tenant_id': instance.tenantId,
      'weak_password': instance.weakPassword,
      'user_name': instance.userName,
      'real_name': instance.realName,
      'avatar': instance.avatar,
      'client_id': instance.clientId,
      'first_login': instance.firstLogin,
      'role_name': instance.roleName,
      'license': instance.license,
      'post_id': instance.postId,
      'user_id': instance.userId,
      'role_id': instance.roleId,
      'nick_name': instance.nickName,
      'oauth_id': instance.oauthId,
      'detail': instance.detail,
      'dept_id': instance.deptId,
      'account': instance.account,
      'jti': instance.jti,
    };

Detail _$DetailFromJson(Map<String, dynamic> json) => Detail(json['type']);

Map<String, dynamic> _$DetailToJson(Detail instance) => <String, dynamic>{
      'type': instance.type,
    };
