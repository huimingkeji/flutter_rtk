// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BannerEntity _$BannerEntityFromJson(Map<String, dynamic> json) => BannerEntity(
      (json['records'] as List<dynamic>)
          .map((e) => BannerInfo.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['total'] as int,
      json['size'] as int,
      json['current'] as int,
      json['orders'] as List<dynamic>,
      json['optimizeCountSql'] as bool,
      json['searchCount'] as bool,
      json['countId'] as String,
      json['maxLimit'] as int,
      json['pages'] as int,
    );

Map<String, dynamic> _$BannerEntityToJson(BannerEntity instance) =>
    <String, dynamic>{
      'records': instance.records,
      'total': instance.total,
      'size': instance.size,
      'current': instance.current,
      'orders': instance.orders,
      'optimizeCountSql': instance.optimizeCountSql,
      'searchCount': instance.searchCount,
      'countId': instance.countId,
      'maxLimit': instance.maxLimit,
      'pages': instance.pages,
    };

BannerInfo _$BannerInfoFromJson(Map<String, dynamic> json) => BannerInfo(
      json['id'] as String,
      json['createUser'] as String,
      json['createDept'] as String,
      json['createTime'] as String,
      json['updateUser'] as String,
      json['updateTime'] as String,
      json['status'] as int,
      json['isDeleted'] as int,
      json['tenantId'] as String,
      json['adName'] as String,
      json['filePath'] as String,
      json['urlPath'] as String,
      json['remark'] as String,
      json['sort'] as int,
      json['adType'] as int,
    );

Map<String, dynamic> _$BannerInfoToJson(BannerInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createUser': instance.createUser,
      'createDept': instance.createDept,
      'createTime': instance.createTime,
      'updateUser': instance.updateUser,
      'updateTime': instance.updateTime,
      'status': instance.status,
      'isDeleted': instance.isDeleted,
      'tenantId': instance.tenantId,
      'adName': instance.adName,
      'filePath': instance.filePath,
      'urlPath': instance.urlPath,
      'remark': instance.remark,
      'sort': instance.sort,
      'adType': instance.adType,
    };
