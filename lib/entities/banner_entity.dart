import 'package:json_annotation/json_annotation.dart';

part 'banner_entity.g.dart';

@JsonSerializable()
class BannerEntity extends Object {
  @JsonKey(name: 'records')
  List<BannerInfo> records;

  @JsonKey(name: 'total')
  int total;

  @JsonKey(name: 'size')
  int size;

  @JsonKey(name: 'current')
  int current;

  @JsonKey(name: 'orders')
  List<dynamic> orders;

  @JsonKey(name: 'optimizeCountSql')
  bool optimizeCountSql;

  @JsonKey(name: 'searchCount')
  bool searchCount;

  @JsonKey(name: 'countId')
  String countId;

  @JsonKey(name: 'maxLimit')
  int maxLimit;

  @JsonKey(name: 'pages')
  int pages;

  BannerEntity(
    this.records,
    this.total,
    this.size,
    this.current,
    this.orders,
    this.optimizeCountSql,
    this.searchCount,
    this.countId,
    this.maxLimit,
    this.pages,
  );

  factory BannerEntity.fromJson(Map<String, dynamic> srcJson) => _$BannerEntityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$BannerEntityToJson(this);
}

@JsonSerializable()
class BannerInfo extends Object {
  @JsonKey(name: 'id')
  String id;

  @JsonKey(name: 'createUser')
  String createUser;

  @JsonKey(name: 'createDept')
  String createDept;

  @JsonKey(name: 'createTime')
  String createTime;

  @JsonKey(name: 'updateUser')
  String updateUser;

  @JsonKey(name: 'updateTime')
  String updateTime;

  @JsonKey(name: 'status')
  int status;

  @JsonKey(name: 'isDeleted')
  int isDeleted;

  @JsonKey(name: 'tenantId')
  String tenantId;

  @JsonKey(name: 'adName')
  String adName;

  @JsonKey(name: 'filePath')
  String filePath;

  @JsonKey(name: 'urlPath')
  String urlPath;

  @JsonKey(name: 'remark')
  String remark;

  @JsonKey(name: 'sort')
  int sort;

  @JsonKey(name: 'adType')
  int adType;

  BannerInfo(
    this.id,
    this.createUser,
    this.createDept,
    this.createTime,
    this.updateUser,
    this.updateTime,
    this.status,
    this.isDeleted,
    this.tenantId,
    this.adName,
    this.filePath,
    this.urlPath,
    this.remark,
    this.sort,
    this.adType,
  );

  factory BannerInfo.fromJson(Map<String, dynamic> srcJson) => _$BannerInfoFromJson(srcJson);

  Map<String, dynamic> toJson() => _$BannerInfoToJson(this);
}
