// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_page_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessagePageEntity _$MessagePageEntityFromJson(Map<String, dynamic> json) => MessagePageEntity(
      ((json['records'] ?? []) as List<dynamic>).map((e) => MessageInfo.fromJson(e as Map<String, dynamic>)).toList(),
      json['total'],
      json['size'],
      json['current'],
      json['orders'],
      json['optimizeCountSql'],
      json['searchCount'],
      json['countId'],
      json['maxLimit'],
      json['pages'],
    );

Map<String, dynamic> _$MessagePageEntityToJson(MessagePageEntity instance) => <String, dynamic>{
      'records': instance.records,
      'total': instance.total,
      'size': instance.size,
      'current': instance.current,
      'orders': instance.orders,
      'optimizeCountSql': instance.optimizeCountSql,
      'searchCount': instance.searchCount,
      'countId': instance.countId,
      'maxLimit': instance.maxLimit,
      'pages': instance.pages,
    };

MessageInfo _$MessageInfoFromJson(Map<String, dynamic> json) => MessageInfo(
      json['id'],
      json['createUser'] ?? "",
      json['createDept'] ?? "",
      json['createTime'] ?? "",
      json['updateUser'] ?? "",
      json['updateTime'] ?? "",
      json['status'] ?? "",
      json['isDeleted'] ?? "",
      json['tenantId'] ?? "",
      json['msgId'] ?? "",
      json['detailCode'] ?? "",
      json['channelId'] ?? "",
      json['channelType'] ?? "",
      json['channelName'] ?? "",
      json['sendAccount'] ?? "",
      json['userId'] ?? "",
      json['receiveAccount'] ?? "",
      json['receiveName'] ?? "",
      json['msgTile'] ?? "",
      json['msgContent'] ?? "",
      json['isRead'] ?? "",
      json['pcUrl'] ?? "",
      json['appUrl'] ?? "",
      json['sourceCode'] ?? "",
      json['sendStatus'] ?? "",
      json['templateCode'] ?? "",
      json['startDate'] ?? "",
      json['endDate'] ?? "",
      json['params'] ?? "",
      json['templateId'] ?? "",
    );

Map<String, dynamic> _$MessageInfoToJson(MessageInfo instance) => <String, dynamic>{
      'id': instance.id,
      'createUser': instance.createUser,
      'createDept': instance.createDept,
      'createTime': instance.createTime,
      'updateUser': instance.updateUser,
      'updateTime': instance.updateTime,
      'status': instance.status,
      'isDeleted': instance.isDeleted,
      'tenantId': instance.tenantId,
      'msgId': instance.msgId,
      'detailCode': instance.detailCode,
      'channelId': instance.channelId,
      'channelType': instance.channelType,
      'channelName': instance.channelName,
      'sendAccount': instance.sendAccount,
      'userId': instance.userId,
      'receiveAccount': instance.receiveAccount,
      'receiveName': instance.receiveName,
      'msgTile': instance.msgTile,
      'msgContent': instance.msgContent,
      'isRead': instance.isRead,
      'pcUrl': instance.pcUrl,
      'appUrl': instance.appUrl,
      'sourceCode': instance.sourceCode,
      'sendStatus': instance.sendStatus,
      'templateCode': instance.templateCode,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
      'params': instance.params,
      'templateId': instance.templateId,
    };
