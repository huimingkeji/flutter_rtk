// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'undo_page_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UndoPageEntity _$UndoPageEntityFromJson(Map<String, dynamic> json) => UndoPageEntity(
      (json['records'] as List<dynamic>).map((e) => UndoItem.fromJson(e as Map<String, dynamic>)).toList(),
      json['total'] as int,
      json['size'] as int,
      json['current'] as int,
      json['orders'] as List<dynamic>,
      json['optimizeCountSql'] as bool,
      json['searchCount'] as bool,
      json['countId'] as String,
      json['maxLimit'] as int,
      json['pages'] as int,
    );

Map<String, dynamic> _$UndoPageEntityToJson(UndoPageEntity instance) => <String, dynamic>{
      'records': instance.records,
      'total': instance.total,
      'size': instance.size,
      'current': instance.current,
      'orders': instance.orders,
      'optimizeCountSql': instance.optimizeCountSql,
      'searchCount': instance.searchCount,
      'countId': instance.countId,
      'maxLimit': instance.maxLimit,
      'pages': instance.pages,
    };

UndoItem _$UndoItemFromJson(Map<String, dynamic> json) => UndoItem(
      json['id'] as String,
      json['businessCode'] as String,
      json['title'] as String,
      json['projectName'] as String,
      json['sourceName'] as String,
      json['sourceCode'] as String,
      json['startUserId'],
      json['startUserName'] as String,
      json['endUserId'],
      json['endUserName'] as String,
      json['startDate'] as String,
      json['endDate'] as String,
      json['thirdInsId'] as String,
      json['insSuccess'] as bool,
      json['insId'] as String,
      json['sendUserId'] as String,
      json['sendUserName'] as String,
      json['sendDate'] as String,
      json['receiverId'] as String,
      json['approverId'],
      json['approverName'] as String,
      json['approveStatus'] as String,
      json['approveTime'] as String,
      json['costTime'] as String,
      json['targetUrl'] as String,
      json['targetMobileUrl'] as String,
      json['success'] as bool,
      json['thirdTaskId'] as String,
      json['remark'] as String,
      json['operationName'] as String,
    );

Map<String, dynamic> _$UndoItemToJson(UndoItem instance) => <String, dynamic>{
      'id': instance.id,
      'businessCode': instance.businessCode,
      'title': instance.title,
      'projectName': instance.projectName,
      'sourceName': instance.sourceName,
      'sourceCode': instance.sourceCode,
      'startUserId': instance.startUserId,
      'startUserName': instance.startUserName,
      'endUserId': instance.endUserId,
      'endUserName': instance.endUserName,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
      'thirdInsId': instance.thirdInsId,
      'insSuccess': instance.insSuccess,
      'insId': instance.insId,
      'sendUserId': instance.sendUserId,
      'sendUserName': instance.sendUserName,
      'sendDate': instance.sendDate,
      'receiverId': instance.receiverId,
      'approverId': instance.approverId,
      'approverName': instance.approverName,
      'approveStatus': instance.approveStatus,
      'approveTime': instance.approveTime,
      'costTime': instance.costTime,
      'targetUrl': instance.targetUrl,
      'targetMobileUrl': instance.targetMobileUrl,
      'success': instance.success,
      'thirdTaskId': instance.thirdTaskId,
      'remark': instance.remark,
      'operationName': instance.operationName,
    };
