// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'collection_tools_page_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CollectionToolsPageEntity _$CollectionToolsPageEntityFromJson(
        Map<String, dynamic> json) =>
    CollectionToolsPageEntity(
      (json['records'] as List<dynamic>)
          .map((e) => CollectionToolsItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['total'] as int,
      json['size'] as int,
      json['current'] as int,
      json['orders'] as List<dynamic>,
      json['optimizeCountSql'] as bool,
      json['searchCount'] as bool,
      json['countId'] as String,
      json['maxLimit'] as int,
      json['pages'] as int,
    );

Map<String, dynamic> _$CollectionToolsPageEntityToJson(
        CollectionToolsPageEntity instance) =>
    <String, dynamic>{
      'records': instance.records,
      'total': instance.total,
      'size': instance.size,
      'current': instance.current,
      'orders': instance.orders,
      'optimizeCountSql': instance.optimizeCountSql,
      'searchCount': instance.searchCount,
      'countId': instance.countId,
      'maxLimit': instance.maxLimit,
      'pages': instance.pages,
    };

CollectionToolsItem _$CollectionToolsItemFromJson(Map<String, dynamic> json) =>
    CollectionToolsItem(
      json['id'] as String,
      json['createUser'] as String,
      json['createDept'] as String,
      json['createTime'] as String,
      json['updateUser'] as String,
      json['updateTime'] as String,
      json['status'] as int,
      json['isDeleted'] as int,
      json['tenantId'] as String,
      json['menuId'] as int,
      json['menuPath'] as String,
      json['menuSource'] as String,
      json['menuNameZh'] as String,
      json['menuNameEn'] as String,
      json['sort'] as int,
      json['userId'] as String,
      json['filePath'] as String,
      json['path'] as String,
      json['projectId'] as String,
      json['projectCode'] as String,
    );

Map<String, dynamic> _$CollectionToolsItemToJson(
        CollectionToolsItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createUser': instance.createUser,
      'createDept': instance.createDept,
      'createTime': instance.createTime,
      'updateUser': instance.updateUser,
      'updateTime': instance.updateTime,
      'status': instance.status,
      'isDeleted': instance.isDeleted,
      'tenantId': instance.tenantId,
      'menuId': instance.menuId,
      'menuPath': instance.menuPath,
      'menuSource': instance.menuSource,
      'menuNameZh': instance.menuNameZh,
      'menuNameEn': instance.menuNameEn,
      'sort': instance.sort,
      'userId': instance.userId,
      'filePath': instance.filePath,
      'path': instance.path,
      'projectId': instance.projectId,
      'projectCode': instance.projectCode,
    };
