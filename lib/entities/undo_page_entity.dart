import 'package:json_annotation/json_annotation.dart';

part 'undo_page_entity.g.dart';

@JsonSerializable()
class UndoPageEntity extends Object {
  @JsonKey(name: 'records')
  List<UndoItem> records;

  @JsonKey(name: 'total')
  int total;

  @JsonKey(name: 'size')
  int size;

  @JsonKey(name: 'current')
  int current;

  @JsonKey(name: 'orders')
  List<dynamic> orders;

  @JsonKey(name: 'optimizeCountSql')
  bool optimizeCountSql;

  @JsonKey(name: 'searchCount')
  bool searchCount;

  @JsonKey(name: 'countId')
  String countId;

  @JsonKey(name: 'maxLimit')
  int maxLimit;

  @JsonKey(name: 'pages')
  int pages;

  UndoPageEntity(
    this.records,
    this.total,
    this.size,
    this.current,
    this.orders,
    this.optimizeCountSql,
    this.searchCount,
    this.countId,
    this.maxLimit,
    this.pages,
  );

  factory UndoPageEntity.fromJson(Map<String, dynamic> srcJson) => _$UndoPageEntityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$UndoPageEntityToJson(this);
}

@JsonSerializable()
class UndoItem extends Object {
  @JsonKey(name: 'id')
  String id;

  @JsonKey(name: 'businessCode')
  String businessCode;

  @JsonKey(name: 'title')
  String title;

  @JsonKey(name: 'projectName')
  String projectName;

  @JsonKey(name: 'sourceName')
  String sourceName;

  @JsonKey(name: 'sourceCode')
  String sourceCode;

  @JsonKey(name: 'startUserId')
  dynamic startUserId;

  @JsonKey(name: 'startUserName')
  String startUserName;

  @JsonKey(name: 'endUserId')
  dynamic endUserId;

  @JsonKey(name: 'endUserName')
  String endUserName;

  @JsonKey(name: 'startDate')
  String startDate;

  @JsonKey(name: 'endDate')
  String endDate;

  @JsonKey(name: 'thirdInsId')
  String thirdInsId;

  @JsonKey(name: 'insSuccess')
  bool insSuccess;

  @JsonKey(name: 'insId')
  String insId;

  @JsonKey(name: 'sendUserId')
  String sendUserId;

  @JsonKey(name: 'sendUserName')
  String sendUserName;

  @JsonKey(name: 'sendDate')
  String sendDate;

  @JsonKey(name: 'receiverId')
  String receiverId;

  @JsonKey(name: 'approverId')
  dynamic approverId;

  @JsonKey(name: 'approverName')
  String approverName;

  @JsonKey(name: 'approveStatus')
  String approveStatus;

  @JsonKey(name: 'approveTime')
  String approveTime;

  @JsonKey(name: 'costTime')
  String costTime;

  @JsonKey(name: 'targetUrl')
  String targetUrl;

  @JsonKey(name: 'targetMobileUrl')
  String targetMobileUrl;

  @JsonKey(name: 'success')
  bool success;

  @JsonKey(name: 'thirdTaskId')
  String thirdTaskId;

  @JsonKey(name: 'remark')
  String remark;

  @JsonKey(name: 'operationName')
  String operationName;

  UndoItem(
    this.id,
    this.businessCode,
    this.title,
    this.projectName,
    this.sourceName,
    this.sourceCode,
    this.startUserId,
    this.startUserName,
    this.endUserId,
    this.endUserName,
    this.startDate,
    this.endDate,
    this.thirdInsId,
    this.insSuccess,
    this.insId,
    this.sendUserId,
    this.sendUserName,
    this.sendDate,
    this.receiverId,
    this.approverId,
    this.approverName,
    this.approveStatus,
    this.approveTime,
    this.costTime,
    this.targetUrl,
    this.targetMobileUrl,
    this.success,
    this.thirdTaskId,
    this.remark,
    this.operationName,
  );

  factory UndoItem.fromJson(Map<String, dynamic> srcJson) => _$UndoItemFromJson(srcJson);

  Map<String, dynamic> toJson() => _$UndoItemToJson(this);
}
