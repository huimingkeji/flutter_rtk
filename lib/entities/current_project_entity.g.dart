// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'current_project_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurrentProjectEntity _$CurrentProjectEntityFromJson(
        Map<String, dynamic> json) =>
    CurrentProjectEntity(
      json['id'] as String,
      json['createUser'] as String,
      json['createDept'] as String,
      json['createTime'] as String,
      json['updateUser'] as String,
      json['updateTime'] as String,
      json['status'] as int,
      json['isDeleted'] as int,
      json['tenantId'] as String,
      json['projectCode'] as String,
      json['projectId'] as String,
      json['deptCode'] as String,
      json['userId'] as String,
      json['mergeName'] as String,
    );

Map<String, dynamic> _$CurrentProjectEntityToJson(
        CurrentProjectEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createUser': instance.createUser,
      'createDept': instance.createDept,
      'createTime': instance.createTime,
      'updateUser': instance.updateUser,
      'updateTime': instance.updateTime,
      'status': instance.status,
      'isDeleted': instance.isDeleted,
      'tenantId': instance.tenantId,
      'projectCode': instance.projectCode,
      'projectId': instance.projectId,
      'deptCode': instance.deptCode,
      'userId': instance.userId,
      'mergeName': instance.mergeName,
    };
