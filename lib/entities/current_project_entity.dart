import 'package:json_annotation/json_annotation.dart';

part 'current_project_entity.g.dart';

@JsonSerializable()
class CurrentProjectEntity extends Object {
  @JsonKey(name: 'id')
  String id;

  @JsonKey(name: 'createUser')
  String createUser;

  @JsonKey(name: 'createDept')
  String createDept;

  @JsonKey(name: 'createTime')
  String createTime;

  @JsonKey(name: 'updateUser')
  String updateUser;

  @JsonKey(name: 'updateTime')
  String updateTime;

  @JsonKey(name: 'status')
  int status;

  @JsonKey(name: 'isDeleted')
  int isDeleted;

  @JsonKey(name: 'tenantId')
  String tenantId;

  @JsonKey(name: 'projectCode')
  String projectCode;

  @JsonKey(name: 'projectId')
  String projectId;

  @JsonKey(name: 'deptCode')
  String deptCode;

  @JsonKey(name: 'userId')
  String userId;

  @JsonKey(name: 'mergeName')
  String mergeName;

  CurrentProjectEntity(
    this.id,
    this.createUser,
    this.createDept,
    this.createTime,
    this.updateUser,
    this.updateTime,
    this.status,
    this.isDeleted,
    this.tenantId,
    this.projectCode,
    this.projectId,
    this.deptCode,
    this.userId,
    this.mergeName,
  );

  factory CurrentProjectEntity.fromJson(Map<String, dynamic> srcJson) => _$CurrentProjectEntityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CurrentProjectEntityToJson(this);
}
