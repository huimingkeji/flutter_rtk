import 'package:connectivity_plus/connectivity_plus.dart';

class ConnectivityService {
  ConnectivityService._internal();

  factory ConnectivityService() => _instance;
  static final ConnectivityService _instance = ConnectivityService._internal();

  // 提供对外访问
  static ConnectivityService to = ConnectivityService();

  bool isOnline = true;

  init() async {
    ConnectivityResult result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.none) {
      isOnline = false;
    } else {
      isOnline = true;
    }
  }
}
