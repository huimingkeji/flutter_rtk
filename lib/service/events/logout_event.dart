import 'package:flutter_rtk/common/common.dart';
import 'package:flutter_rtk/common/constants.dart';
import 'package:flutter_rtk/main.dart';
import 'package:flutter_rtk/router/index.dart';
import 'package:flutter_rtk/service/store_service.dart';

class LogoutEvent {
  String message;

  LogoutEvent(this.message);

  static void logout(String message) async {
    await StoreService.to.remove(AppConstants.userIsLogin);
    if (message.isNotEmpty) {
      showToast(message);
    }
    globalNavigatorKey.currentState?.pushNamedAndRemoveUntil(AppRouters.login, (router) => false);
  }
}
