import 'package:shared_preferences/shared_preferences.dart';

class StoreService {
  StoreService._internal();

  factory StoreService() => _instance;
  static final StoreService _instance = StoreService._internal();

  // 提供对外访问
  static StoreService to = StoreService();

  //
  late SharedPreferences _prefs;

  init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  setString(String key, String v) async {
    await _prefs.setString(key, v);
  }

  String getString(String key) {
    return _prefs.getString(key) ?? "";
  }

  String getStringByDefaultValue(String key, String defaultValue) {
    return _prefs.getString(key) ?? defaultValue;
  }

  setBool(String key, bool v) async {
    await _prefs.setBool(key, v);
  }

  bool getBool(String k) {
    return _prefs.getBool(k) ?? false;
  }

  setInt(String k, int v) async {
    await _prefs.setInt(k, v);
  }

  int getInt(String k) {
    return _prefs.getInt(k) ?? 0;
  }

  remove(String k) {
    return _prefs.remove(k);
  }

  clear() async {
    return await _prefs.clear();
  }
}
