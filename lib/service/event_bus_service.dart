import 'package:event_bus/event_bus.dart';
import 'package:flutter_rtk/service/events/logout_event.dart';
import 'package:flutter_rtk/service/events/watch_cdt_event.dart';

class EventBusService {
  EventBusService._internal();

  factory EventBusService() => _instance;
  static final EventBusService _instance = EventBusService._internal();

  static late EventBus _eventBus;

  // cdt是否可以监听
  static bool _isCanWatchCDT = false;

  static bool get isCanWatchCDT => _isCanWatchCDT;

  static fire(event) {
    _eventBus.fire(event);
  }

  static init() {
    _eventBus = EventBus();
    _eventBus.on().listen((event) {
      // 退出事件
      if (event is LogoutEvent) {
        LogoutEvent.logout(event.message);
      }
      // cdt监测事件
      if (event is WatchCDTEvent) {
        _isCanWatchCDT = event.isCanWatch;
      }
    });
  }

  // static void logout(String message) async {
  //   await StoreService.to.remove(AppConstants.userIsLogin);
  //   if (message.isNotEmpty) {
  //     showToast(message);
  //   }
  //   globalNavigatorKey.currentState?.pushNamedAndRemoveUntil(AppRouters.login, (router) => false);
  // }

  static void remove() {
    _eventBus.destroy();
  }
}
