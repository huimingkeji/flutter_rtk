import 'package:flutter/material.dart';
import 'package:flutter_rtk/entities/login_entity.dart';
import 'package:flutter_rtk/entities/user_entity.dart';

class UserInfoModel extends ChangeNotifier {
  // token
  LoginEntity? loginInfo;

  // user info
  late UserEntity userInfo;

  loginSuccess(LoginEntity loginEntity) {
    loginInfo = loginEntity;
    notifyListeners();
  }

  setUserInfo(UserEntity userEntity) {
    userInfo = userEntity;
    notifyListeners();
  }
}
