import 'package:flutter/material.dart';
import 'package:flutter_rtk/entities/banner_entity.dart';
import 'package:flutter_rtk/entities/collection_tools_page_entity.dart';
import 'package:flutter_rtk/entities/current_project_entity.dart';
import 'package:flutter_rtk/entities/message_page_entity.dart';
import 'package:flutter_rtk/entities/project_entity.dart';
import 'package:flutter_rtk/entities/tools_entity.dart';
import 'package:flutter_rtk/entities/undo_page_entity.dart';

class AppModel extends ChangeNotifier {
  // 初始页面
  int currentIdx = 0;
  late PageController _controller;

  // 首页
  // List<BannerInfo> banners = [];
  BannerEntity? bannerEntity;
  List<ProjectEntity> projects = [];
  UndoPageEntity? undoTaskInfo;
  CurrentProjectEntity? currentProjectInfo;
  MessagePageEntity? unReadMessagePage;

  // 工具界面相关
  bool appPageIsEdit = false;
  List<ToolsEntity> tools = [];
  List<ToolsEntity> collectionTools = [];

  changeCurrentIdx(int index, {bool isEdit = false}) {
    appPageIsEdit = isEdit;
    currentIdx = index;
    _controller.jumpToPage(currentIdx);
    notifyListeners();
  }

  changeEditState() {
    appPageIsEdit = !appPageIsEdit;
    notifyListeners();
  }

  initController(controller) {
    _controller = controller;
  }

  setAllProjects(List<ProjectEntity> projectList) {
    projects = projectList;
    notifyListeners();
  }

  setCurrentProject(CurrentProjectEntity currentProjectEntity) {
    currentProjectInfo = currentProjectEntity;
    notifyListeners();
  }

  setBannerInfo(BannerEntity banner) {
    bannerEntity = banner;
  }

  setUndoTaskInfo(UndoPageEntity? undoTaskEntity) {
    if (undoTaskEntity == null) {
      return;
    }
    undoTaskInfo = undoTaskEntity;
    notifyListeners();
  }

  setAllTools(List<ToolsEntity> toolList) {
    tools = toolList;
    notifyListeners();
  }

  setCollectionTools(CollectionToolsPageEntity pageEntity) {
    if (pageEntity.size > 0) {
      // collection tools ==> tools
      collectionTools = pageEntity.records.map((CollectionToolsItem e) => ToolsEntity(e.id, "0", "", e.menuNameZh, "--", e.path, e.menuSource, e.sort, -1, -1, 0, e.status, "", -1, "", e.filePath, e.isDeleted, "", "", -1, "", "", [], false, "", "", "", "")).toList();
    }
    notifyListeners();
  }

  resetCurrentIdx() {
    currentIdx = 0;
    notifyListeners();
  }

  setUnreadMessageInfo(MessagePageEntity? messagePageInfo) {
    if (messagePageInfo == null) {
      return;
    }
    unReadMessagePage = messagePageInfo;
    notifyListeners();
  }
}
