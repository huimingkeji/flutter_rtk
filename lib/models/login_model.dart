import 'package:flutter/material.dart';

class LoginModel extends ChangeNotifier {
  // -1: error 0: unloading 1:loading
  int isLoadScanCode = 0;
  Image? qrCodeImage;

  // 状态：1、未鉴权，2、已授权，3、已登录，4、已失效
  int codeStatus = 1;

  changeCodeStatus(int status) {
    codeStatus = status;
    notifyListeners();
  }

  changeLoadStatus(int status) {
    isLoadScanCode = status;
    notifyListeners();
  }

  setImage(Image image) {
    qrCodeImage = image;
    notifyListeners();
  }
}
