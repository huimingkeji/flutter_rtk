package com.rtk.flutter_rtk;

import com.southgnss.sdk4business.SouthSDK;

import io.flutter.app.FlutterApplication;

public class MainApplication extends FlutterApplication {
    private static MainApplication application;

    private boolean isSetting = false;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        SouthSDK.init(this, "01SDKTest", 1011);
    }

    public static MainApplication getApplication() {
        return application;
    }

    public boolean isSetting() {
        return isSetting;
    }

    public void setSetting(boolean setting) {
        isSetting = setting;
    }
}
