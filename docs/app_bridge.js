function FlutterInvoker() {
    let variableNum = 0
    window.flutterBridgeEventObj = {}
    this.call = function (method, data, callBack) {
        variableNum++
        window.flutterBridgeEventObj[`callBack${variableNum}`] = callBack
        const postData = JSON.stringify({
            method,
            data: JSON.stringify(data),
            callback: `window.flutterBridgeEventObj.callBack${variableNum}`
        })
        // call flutter api
        try {
            FlutterApi.postMessage(postData);
        } catch (_) {
        }
    }
}
function init() {
    window.FlutterInvoker = new FlutterInvoker();
}
// 将 AppApi 对象赋值给 window.AppApi，以便全局使用
init()